### This is just a sample of how to login to PostGresql DB, Query data and write to Excel Spreadsheet,
### specific to different Worksheets. 

from datetime import datetime
#PostgreSQL db adapter for Python
import psycopg2
import xlsxwriter
import sys

query_active_users = """
        SELECT DISTINCT users.Id as "Id", accounts.Id as "AccountId", users.email as "Username", organizations.name as "Organization", accounts.name as "Account", license_organizations."licenseType" as "Account License", 
        to_char(license_organizations."createdAt", 'YYYY-MM-DD HH24:MI:SS') as "AccountStartDate", to_char(license_organizations.expiration, 'YYYY-MM-DD HH24:MI:SS') as "AccountExpirationDate"
        from organizations
		inner join accounts on organizations.id = accounts.organization
		inner join license_devices on accounts.id = license_devices.account
		inner join license_organizations on organizations.id = license_organizations.organization
		inner join account_permissions on accounts.id = account_permissions.account
		inner join users on account_permissions."user" = users.id
		where license_organizations.expiration > now() and users."isActive" = 'true'
		order by organizations.name
        """
user_logins_dates_dset = []

# Connect to PostGre IdenService DB
def query_iden_service(q):
    hostname = 'HostIP'
    username = 'FakeUsername'
    password = 'FakePassword'
    database = 'iden_service'
    conn = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = conn.cursor()
    cur.execute(q)
    dset = cur.fetchall()
    conn.close()
    return dset

def write_to_xcel_file():
    active_users_dset = (query_iden_service(query_active_users))

    wb = xlsxwriter.Workbook('./User_Access.xlsx')
    user_access_ws = wb.add_worksheet('Active Users')
    bold = wb.add_format({'bold': 1})
    user_access_ws.set_column(0, 5, 35)
    user_access_ws.write('A1', 'UserName', bold)
    user_access_ws.write('B1', 'Organization', bold)
    user_access_ws.write('C1', 'Account', bold)
    user_access_ws.write('D1', 'AccountLicense', bold)
    user_access_ws.write('E1', 'AccountStartDate', bold)
    user_access_ws.write('F1', 'AccountExpirationDate', bold)
    user_access_ws.write('G1', 'Last Login Date', bold)
    user_access_ws.write('H1', 'Last Account API Call Date', bold)
    user_access_ws.write('I1', '7 Day Login Count', bold)
    user_access_ws.write('J1', '7 Day Account API Count', bold)

    row=1
    col=0
    for active_user in active_users_dset:
        user_access_ws.write_string(row, col, active_user[2])
        user_access_ws.write_string(row, col+1, active_user[3])
        user_access_ws.write_string(row, col + 2, active_user[4])
        user_access_ws.write_string(row, col + 3, active_user[5])
        user_access_ws.write_string(row, col + 4, active_user[6])
        user_access_ws.write_string(row, col + 5, active_user[7])

        userId = active_user[0]
        accountId = active_user[1]

        last_login_date = get_last_user_login_datetime(userId)
        if last_login_date == None:
            last_login_date = "Never"
        user_access_ws.write_string(row, col + 6, last_login_date)

        last_api_call_date = get_last_api_call_datetime(accountId)
        if last_api_call_date == None:
            last_api_call_date = "Never"
        user_access_ws.write_string(row, col + 7, last_api_call_date)

        login_count = get_login_count(userId)
        user_access_ws.write_number(row, col + 8, login_count)

        api_count = get_api_call_count(accountId)
        user_access_ws.write_number(row, col + 9, api_count)

        row+=1

    product_lane_dset = (query_identity_service(query_product_lane))
    user_access_ws = wb.add_worksheet('Product Lane Access')
    bold = wb.add_format({'bold': 1})
    user_access_ws.set_column(0, 5, 35)
    user_access_ws.write('A1', 'UserName', bold)
    user_access_ws.write('B1', 'Organization', bold)
    user_access_ws.write('C1', 'Account', bold)
    user_access_ws.write('D1', 'ProductLane', bold)
    user_access_ws.write('E1', 'Visibility', bold)
    user_access_ws.write('F1', 'AccountAccess', bold)
    user_access_ws.write('G1', 'LaneType', bold)

    row=1
    col=0
    for product_lane in product_lane_dset:
        user_access_ws.write_string(row, col, product_lane[0])
        user_access_ws.write_string(row, col+1, product_lane[1])
        user_access_ws.write_string(row, col + 2, product_lane[2])
        user_access_ws.write_string(row, col + 3, product_lane[3])
        user_access_ws.write_string(row, col + 4, product_lane[4])
        user_access_ws.write_string(row, col + 5, product_lane[5])
        user_access_ws.write_string(row, col + 6, product_lane[6])
        row+=1

    wb.close()

def get_last_user_login_datetime(userId):
    global user_logins_dates_dset
    if len(user_logins_dates_dset) == 0:
        user_logins_dates_dset = query_audit_service(query_last_user_login)

    for user_login in user_logins_dates_dset:
        if int(user_login[1]) == userId:
            return user_login[0]

    return None



write_to_xcel_file()