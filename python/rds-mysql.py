# Create RDS Instance and EC2 Instance first
# Check these on EC2 instances
# sudo apt install mysql-client
# # sample employees database file
# wget https://storage.googleapis.com/skl-training/aws-codelabs/rds/employees.sql
# mysql -h rdshostname -u root -p   #it asks for Password 
# if it hangs after that, check Security Group and provide your VPC CIDR block
# use employees
# # Generate data from the sql file now
# source employees.sql
# # fetching data
# show databases;
# describe employees;
# select * from employees limit 10;
# select count(1) from employees;

# python3 -V
# sudo apt install python-pip3
# pip3 install boto3
# sudo pip3 install mysql-connector-python
# sudo pip3 install pymysql

import pymysql
import mysql.connector

# Get the hostname from your RDS instance
hostname = 'hostname'
# Get the MySQL username and password that you set on the RDS instance 
username = 'username'
password = 'password'
database = 'employees'

def makeQuery(conn):
    cur = conn.cursor()
    cur.execute("Select emp_no, first_name, last_name from employees limit 10")
    for eno, fname, lname in cur.fetchall():
        print (eno, fname, lname)

print ("Using pymysql")
myConn = pymysql.connect(host=hostname, user=username, passwd=password, db=database)
makeQuery(myConn)
myConn.close()

print ("\n\nUsing mysql.connector")
myConn = mysql.connector.connect(host=hostname, user=username, passwd=password, db=database)
makeQuery(myConn)
myConn.close()