from datetime import datetime
from datetime import timedelta
from elasticsearch import Elasticsearch
from slacker import Slacker
import xlsxwriter

def elastic_conn():
    uname = 'FakeUsername'
    pwd = 'FakePassword'
    ca_cert = '/Users/user/Documents/JobServer/ControllerLogsAlerting/Certificate/ca-cntrllog.crt'

    client = Elasticsearch(
      ['10.144.800.12', '10.144.800.13', '10.144.800.14', '10.144.800.15'],
      http_auth=(uname,pwd),
      port=9200,
      use_ssl=True,
      verify_certs=True,
      ca_certs=ca_cert,
    )
    return client

def main_query(dte, client, machine_names, exception_names, lev):
    mess="C001:T1, C002:T2, C016:T3, C017:Mal-Http, C018:Sub, C022:Abux"+"\n"
    mess+="UTC:  "+str(datetime.utcnow())+"\n"
    mess+="Local:"+str(datetime.now())+"\n"+"\n"
    mess+="FATAL LOGS (PAST 30 MINUTES FROM UTC/Local)"+"\n"
    # Create a workbook and add a worksheet.
    wb = xlsxwriter.Workbook(
        '/Users/user/Documents/JobServer/ControllerLogsAlerting/ControllerLogs.xlsx')
    logs_ws = wb.add_worksheet('FatalLogs')
    # Add a bold format to use to highlight cells.
    bold = wb.add_format({'bold': 1})
    # Adjust the column width.
    logs_ws.set_column(0, 0, 15)
    logs_ws.set_column(1, 1, 60)
    logs_ws.set_column(2, 2, 15)
    # Write some data headers.
    logs_ws.write('A1', 'MachineName', bold)
    logs_ws.write('B1', 'Exception', bold)
    logs_ws.write('C1', 'NoOfOccurences', bold)
    row=1
    col=0
    for m in machine_names:
        for e in exception_names:
            try:
                response = client.search(
                    body={
                        "query": {
                            "bool": {
                                "must": [
                                    {"match": {"level": lev}},
                                    {"match": {"exceptionType.keyword": e}},
                                    {"term": {"machinename.keyword": m}}
                                ],
                                "filter": [
                                    {"range": {"@timestamp": {"gte": dte}}}
                                ]
                            }
                        }
                    }
                )
                if response["hits"]:
                    NoOfRecords = 0
                    NoOfRecords = int(response["hits"]["total"])
                    if NoOfRecords > 0:
                        # Start from the first cell below the headers
                        logs_ws.write_string(row, col, m)
                        logs_ws.write_string(row, col+1, e)
                        logs_ws.write_string(row, col+2, str(NoOfRecords))

                        mess+=m+" - "+e+" - "+str(NoOfRecords)+"\n"

                    row = row + 1

                else:
                    NoOfRecords = int(0)


            except Exception as ex:
                print ("error:", ex)
    return mess

def distinct_machinename(dte, client, lev):
    try:
        response = client.search(
            body={
                "_source": ["machinename", "exceptionType"],
                "query":
                  {
                    "bool":
                    {
                      "must": [
                        {
                          "match": {
                            "level": lev
                          }
                        }
                      ],
                      "filter":
                      {
                        "range": {
                          "@timestamp": {
                            "gte": dte
                          }
                        }
                      }
                    }
                  },

                  "aggs" : {
                  "uniq_mac" : {
                  "terms" : { "field" : "machinename.keyword" }
                  }
                  }
                }
        )
    except Exception as ex:
        print ("error:", ex)

    mname=[]
    for mac in response['aggregations']['uniq_mac']['buckets']:
        NoOfRecords = 0
        NoOfRecords = int(mac['doc_count'])
        if NoOfRecords > 0:
            mname.append(mac['key'])

    return mname

def distinct_exception(dte, client, lev):
    try:
        response = client.search(
            body={
                "_source": ["machinename", "exceptionType"],
                "query":
                  {
                    "bool":
                    {
                      "must": [
                        {
                          "match": {
                            "level": lev
                          }
                        }
                      ],
                      "filter":
                      {
                        "range": {
                          "@timestamp": {
                            "gte": dte
                          }
                        }
                      }
                    }
                  },

                  "aggs" : {
                  "uniq_exception" : {
                  "terms" : { "field" : "exceptionType.keyword" }
                  }
                  }
                }
        )
    except Exception as ex:
        print ("error:", ex)

    excname=[]
    for mac in response['aggregations']['uniq_exception']['buckets']:
        NoOfRecords = 0
        NoOfRecords = int(mac['doc_count'])
        if NoOfRecords > 0:
            excname.append(mac['key'])

    return excname

def post_slack(bod1):
	i = datetime.now()
	dtime = i.strftime('%Y/%m/%d %H:%M:%S')
	j = datetime.utcnow()
	utctime = j.strftime("%Y/%m/%d")
	"""Post slack message."""
	try:
		token = 'xoxp-52916255797-64128479392-1518bbe287bd7b41'
		slack = Slacker(token)

		obj = slack.chat.post_message(
			#channel='#caws_controllers',
			channel='#caws_metrics_test',
			text='',
			as_user=True,
			attachments=[{"text": bod1}])
		print (obj.successful, obj.__dict__['body']['channel'], obj.__dict__['body']['ts'])
	except KeyError as ex:
            print ('Environment variable %s not set.' % str(ex))

def main():
    utc_time = datetime.utcnow()
    #Getting the data from now-30 minutes
    utc_delta = str(datetime.utcnow()-timedelta(minutes=30))
    split_before = utc_delta.split(' ')[0]
    split_after = utc_delta.split(' ')[1]
    dte = split_before+"T"+split_after
    #gte_time format = "2017-09-25T12:00:00.000000"
    distinct_exception(dte, elastic_conn(),"fatal")
    machine_names = distinct_machinename(dte,elastic_conn(), "fatal")
    exception_names = distinct_exception(dte, elastic_conn(), "fatal")
    print(main_query(dte,elastic_conn(),machine_names,exception_names, "fatal"))
    post_slack(main_query(dte,elastic_conn(),machine_names,exception_names, "fatal"))

main()