from datetime import datetime
from datetime import timedelta
from elasticsearch import Elasticsearch
import os

#Working
#result_cl = client.cluster.stats()
#size = result_cl['nodes']['jvm']['mem']

uname = 'FakeUsername'
pwd = 'FakePassword'
ca_cert = 'C:\ControllerLogsAlerting\Certificate\ca-cntrllog.crt'


client = Elasticsearch(
  ['10.188.800.15'],
  http_auth=(uname,pwd),
  port=9200,
  use_ssl=True,
  verify_certs=True,
  ca_certs=ca_cert,
)

print (client.cluster.health())
result = client.cluster.health()
print ('\n ****************** \n')
print (str(result[u'status']))
print ('\n ****************** \n')

result_cl = client.nodes.stats()
#print (result_cl)
print (str(result_cl[u'cluster_name']))
print ('\n ****************** \n')
print (str(result_cl[u'_nodes']))

print ('\n THREAD POOL \n')
print ('\n ****************** \n')
print (result_cl['nodes']['6WHrSiiER_ai1jDhaFTX4A']['thread_pool']['bulk'])
print ('\n TRANSPORT \n')
print ('\n ****************** \n')
print (result_cl['nodes']['6WHrSiiER_ai1jDhaFTX4A']['transport'])
print ('\n WATCHER \n')
print ('\n ****************** \n')
print (result_cl['nodes']['6WHrSiiER_ai1jDhaFTX4A']['thread_pool']['watcher'])
print ('\n IO_STATS \n')
print ('\n ****************** \n')
print (result_cl['nodes']['6WHrSiiER_ai1jDhaFTX4A']['fs']['io_stats']['devices'])