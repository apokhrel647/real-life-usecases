function shut-controllers
{
    param($controller)

    if($($DefaultVIServer.Name) -ne "onrvcsa-play.mgt.colo1.labs.com")
    {
        Connect-VIServer onrvcsa-play.mgt.colo1.labs.com -Protocol https -Credential $global:cred
    }
    function Get-ProcessId{
        param($controllerName)

        $script=@"
`$reg= Get-Process | Where-Object {`$_.ProcessName -eq `'tpxprocess'} | select Id | Select -First 1
function print-json {
param(
`$theThing
)
`$properties = `$theThing | get-member -type NoteProperty
`$json = "{"
foreach (`$property in `$properties) {
`$name = `$property.name
`$json +="``"`$(`$name)``":``"`$(`$theThing.(`$name).Tostring().Replace('\','\\'))``","
}
`$json = `$json.trim(',')+'}'
write-host `$json
}
print-json -theThing `$reg
"@
        try {
            $res = Invoke-VMScript -VM $controllerName -ScriptType Powershell  $script -GuestUser FakeUsername -GuestPassword "FakePassword" | ConvertFrom-Json
            $Id = $($res.Id) 
            return ($Id)  
        }
        catch {
            return (0)
        }
    }
    $id = Get-ProcessId -controllerName $controller
    Write-Host "`nController : $controller" -ForegroundColor Blue

    if($id -ne 0)
    {
    Write-Host "The Process is running with ProcessId $id"  -ForegroundColor Blue
    Write-Host "About to kill the Process with ProcessId $id" -ForegroundColor Blue
        Invoke-VMScript -VM $controller -ScriptType Powershell  'Get-Process | Where-Object {$_.ProcessName -eq "powershell"} | Stop-Process -Force' -GuestUser FakeUsername -GuestPassword "FakePassword" | Out-Null
        Invoke-VMScript -VM $controller -ScriptType Powershell  'Get-Process | Where-Object {$_.ProcessName -eq "tpxprocess"} | Stop-Process -Force' -GuestUser FakeUsername -GuestPassword "FakePassword" | Out-Null
        $id1 = Get-ProcessId -controllerName $controller
        if($id1 -ne 0)
        {
        Write-Host "Sorry, the Process on $controller couldn't be stopped. Do it Manually." -ForegroundColor Red
        }
        else
        {
        Write-Host "The Process on $controller has been stopped successfully." -ForegroundColor Blue
        }

    }
    else
    {
    Write-Host "The Process is already not running on $controller" -ForegroundColor Blue
    }   
}


Export-ModuleMember -Function shut-controllers