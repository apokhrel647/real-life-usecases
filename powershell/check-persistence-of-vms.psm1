function check-persistence
{
   if ($($DefaultVIServer.Name) -notmatch "$global:testenv")
   {
      Connect-VIServer $global:vcenter -Protocol https -Credential $global:cred
   }
   Write-Host "`nChecking Persistence of Victims" -ForegroundColor Yellow
   foreach($lane in $global:victims)
   {
      $per_status = get-vm "$($lane.VMNAME)" | Get-HardDisk | where {$_.Persistence -eq "IndependentPersistent"} 
      if(!$per_status) 
      {
         Write-Host  "$($lane.Product) - $($lane.VMName) - Non-Persistent" -ForegroundColor Blue
      }    
      else 
      {
      Write-Host  "$($lane.Product) - $($lane.VMName) - Persistent" -ForegroundColor Red
      }
   }
    
}
Export-ModuleMember -Function check-persistence


