[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls -bor [Net.SecurityProtocolType]::Tls11 -bor [Net.SecurityProtocolType]::Tls12
##Parameters
#$index="caws_ngfw"
$index="bps_3"
$message="FG100FTK19000185"
$greaterThanEqual="2020-05-26T00:00:00.000Z"
$lessThan="2020-07-24T00:00:00.000Z"

if ("TrustAllCertsPolicy" -as [type]) {} else {
    Add-Type "using System.Net;using System.Security.Cryptography.X509Certificates;public class TrustAllCertsPolicy : ICertificatePolicy {public bool CheckValidationResult(ServicePoint srvPoint, X509Certificate certificate, WebRequest request, int certificateProblem) {return true;}}"
    [System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
}
$AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
$interval=-join("now-",$day,"d")
$user = "FakeUsername"
$pass = "FakePassword"
$secpasswd = ConvertTo-SecureString $pass -AsPlainText -Force
$Credential = New-Object System.Management.Automation.PSCredential ($user , $secpasswd)
$elasticUri = "https://syslog.labs.com:9200/$index/_search"

$elasticQuery = @"
{  "size":0, "track_total_hits": true,  "_source": [    "_index",    "shost",    "host",    "message",    "@timestamp"  ],  "query": {    "bool": {      "must": [        {          "query_string": {            "fields": [              "message"            ],            "query": "$message",            "minimum_should_match": "100%"          }        }      ],      "filter": {        "range": {          "@timestamp": {            "gte": "$greaterThanEqual",            "lt": "$lessThan"          }        }      }    }  }}
"@

$response = Invoke-RestMethod -Uri $elasticUri -Method Post -ContentType 'application/json' -Body $elasticQuery -Certificate "C:\Scripts\smoketestfinal\privates\syslogcertificate.crt" -Credential $Credential
$output=0
$output= ($($($response.hits).total).value)
Write-Host "Found $output Hits `n" -ForegroundColor Blue

$elasticQuery = @"
{  "size":$output, "track_total_hits": true,  "_source": [    "_index",    "shost",    "host",    "message",    "@timestamp"  ],  "query": {    "bool": {      "must": [        {          "query_string": {            "fields": [              "message"            ],            "query": "$message",            "minimum_should_match": "100%"          }        }      ],      "filter": {        "range": {          "@timestamp": {            "gte": "$greaterThanEqual",            "lt": "$lessThan"          }        }      }    }  }}
"@
$response = Invoke-RestMethod -Uri $elasticUri -Method Post -ContentType 'application/json' -Body $elasticQuery -Certificate "C:\Scripts\smoketestfinal\privates\syslogcertificate.crt" -Credential $Credential
$hits=$null
$hits=$($($response.hits).hits)._source

$int=0
foreach($h in $hits)
{
    $int=$int+1
    Write-host "$int ---------------- $($h.'@timestamp') ----------------  $($h.host) ---------------- $($h.message) `n" -ForegroundColor Yellow

}
