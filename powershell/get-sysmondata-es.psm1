function get-sysmondata {
  param ($sessionToken)
  
  $index=$null
  
  $TimeNow=Get-Date
  Get-Date $TimeNow -f "MM-dd-yy" | Out-Null
  $index = $TimeNow.ToUniversalTime().ToString("MM-dd-yy")

  if (-not ([System.Management.Automation.PSTypeName]'ServerCertificateValidationCallback').Type)
  {
  $certCallback = @"
      using System;
      using System.Net;
      using System.Net.Security;
      using System.Security.Cryptography.X509Certificates;
      public class ServerCertificateValidationCallback
      {
          public static void Ignore()
          {
              if(ServicePointManager.ServerCertificateValidationCallback ==null)
              {
                  ServicePointManager.ServerCertificateValidationCallback += 
                      delegate
                      (
                          Object obj, 
                          X509Certificate certificate, 
                          X509Chain chain, 
                          SslPolicyErrors errors
                      )
                      {
                          return true;
                      };
              }
          }
      }
"@
  Add-Type $certCallback
}
[ServerCertificateValidationCallback]::Ignore()


$user = "FakeUsername"
$pass = "FakePassword"
$secpasswd = ConvertTo-SecureString $pass -AsPlainText -Force
$Credential = New-Object System.Management.Automation.PSCredential ($user , $secpasswd)
#$elasticUri = "https://syslog.labs.com:9200/sysmon*/_search"
$elasticUri = "https://syslog.labs.com:9200/sysmon-scale-$index/_search"
$elasticQuery = @"
{"track_total_hits":"true",  "_source": ["hostname","timestamp","session_token" ],  "size": 20,   "query":   {    "term":     {    "session_token": "$sessionToken"    }  }}
"@
  $response = Invoke-RestMethod -Uri $elasticUri -Method Post -ContentType 'application/json' -Body $elasticQuery -Certificate "$global:workingdir\privates\syslogcertificate.crt" -Credential $Credential
  return ($($($response.hits).total).value)
}


Export-ModuleMember -Function get-sysmondata