[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Import-Module PSSpark
function write-webex
{
param($text)

$creds = $global:settings
$bottoken =$creds.bottoken
$rID=$creds.roomid

$body = @{
roomId=$rID
text=$text
}
$json=$body | ConvertTo-Json
Invoke-RestMethod -Method Post `
-Headers @{"Authorization"="Bearer $bottoken"} `
-ContentType "application/json" -Body $json `
-Uri "https://api.ciscospark.com/v1/messages"`
}

Export-ModuleMember -Function write-webex