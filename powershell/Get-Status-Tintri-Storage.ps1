[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$cutoffpercentage=25

Import-Module 'C:\Program Files\TintriPSToolkit\TintriPSToolkit.psd1'
If (!(Get-module "VmWare.VimAutomation.Core")) {Import-Module "VmWare.VimAutomation.Core" -Force}

##Test Lab
$tintriServer1='tintri-01.testlab.labs.com'

$user='admin'
$tlb_passowrd1='FakePassword'

$cred_tlb1 = New-Object -Typename System.Management.Automation.PSCredential -ArgumentList $user,($tlb_passowrd1 | ConvertTo-SecureString -AsPlainText -Force)

$tintri01=Connect-TintriServer -Server $tintriServer1 -Credential $cred_tlb1 -IgnoreCertificateWarnings

$tlbt1= $tintri01 | Get-TintriDashboard

$tlbt1_ts = [math]::Round($($tlbt1.SpaceTotalGiB))
$tlbt1_fs = [math]::Round($($tlbt1.SpaceFreeGib))
$tlbt1_fsp = [math]::Round(($tlbt1_fs/$tlbt1_ts)*100)
$tlbt1_sp = [math]::Round($($tlbt1.SpaceProvisioned))
$tlbt1_fp = 100-[math]::Round($($tlbt1.SpaceProvisionedPercent))

##DC
$dc5060_1='onrt5060-01.mgt.colo1.labs.com'

$user='admin'
$dc5060_1_pwd='hZ7/A8Z9$ZC'

$cred_dc5060_1 = New-Object -Typename System.Management.Automation.PSCredential -ArgumentList $user,($dc5060_1_pwd | ConvertTo-SecureString -AsPlainText -Force)

$tin_dc5060_1=Connect-TintriServer -Server $dc5060_1 -Credential $cred_dc5060_1  -IgnoreCertificateWarnings

$5060_1= $tin_dc5060_1 | Get-TintriDashboard

$5060_1_ts = [math]::Round($($5060_1.SpaceTotalGiB))
$5060_1_fs = [math]::Round($($5060_1.SpaceFreeGib))
$5060_1_fsp = [math]::Round(($5060_1_fs/$5060_1_ts)*100)
$5060_1_sp = [math]::Round($($5060_1.SpaceProvisioned))
$5060_1_fp = 100-[math]::Round($($5060_1.SpaceProvisionedPercent))

# Write-Host "FreeSpace%, TintriServer (VMCount), TotalSpaceGB, FreeSpaceGB, ProvSpaceGB, ProvFreeSpace%" -ForegroundColor Yellow
# Write-Host "$5060_1_fsp%, $dc5060_1 $($5060_1.Model) ($($5060_1.VMCount)), $5060_1_ts, $5060_1_fs, $5060_1_sp, $5060_1_fp%" -ForegroundColor DarkMagenta

$dte=Get-Date -Format "yyyy-MM-dd HH:mm"
$output=$null
$output+= "`nTintri VMStore Monitoring : $dte " 
$output+= "`n`nFreeSpace%, TintriServer(VMCount), TotalSpaceGB, FreeSpaceGB, ProvSpaceGB, ProvFreeSpace%" 
if($5060_1_fsp -lt $cutoffpercentage)
{
    $output+="`n-->`n$5060_1_fsp%, $dc5060_1 $($5060_1.Model) ($($5060_1.VMCount)), $5060_1_ts, $5060_1_fs, $5060_1_sp, $5060_1_fp%"
    write-webexAlert "Caution - $dte : Tintri Space less than $cutoffpercentage%`n$5060_1_fsp%, $dc5060_1 $($5060_1.Model) ($($5060_1.VMCount)), $5060_1_ts, $5060_1_fs, $5060_1_sp, $5060_1_fp%"
}else {
    $output+="`n$5060_1_fsp%, $dc5060_1 $($5060_1.Model) ($($5060_1.VMCount)), $5060_1_ts, $5060_1_fs, $5060_1_sp, $5060_1_fp%"
}

write-webex $output