function submit-apirequestall
{
    param($samples)
    $emailsamples=$null
    $httpsamples=$null
    $fpsamples=$null
    $exploitsamples=$null
    $cor_id=$null
    $productcategory="Advanced Endpoint Protection"
    $dte=Get-Date -Format "ddMMyyyy_hh_mm"
    if($samples -eq 'email')
    {
        If(Test-Path $workingdir\correlationids\email.csv)
        {
            Rename-Item -Path $workingdir\correlationids\email.csv -NewName "email_$dte.csv" -Confirm:$false -Force
        }
        $emailsamples=Get-Content $workingdir\samples\email.txt
        New-Item -Path $workingdir\correlationids\email.csv -Force 
        $res="device,correlationid"
        $res |  Out-File "$workingdir\correlationids\email.csv" -Append -Force -Encoding UTF8
        foreach($e in $emailsamples)
        {
            Write-Host "$e"
            $email_sub = submit-malwareapi -captureid $e -product $productcategory
            $cor_id = $($email_sub.correlationId)
            $res="$global:device,$cor_id"
            $res |  Out-File "$workingdir\correlationids\email.csv" -Append -Force -Encoding UTF8
        }
    }elseif ($samples -eq 'http') {
        If(Test-Path $workingdir\correlationids\http.csv)
        {
            Rename-Item -Path $workingdir\correlationids\http.csv -NewName "http_$dte.csv" -Confirm:$false -Force
        }
        $httpsamples=Get-Content $workingdir\samples\http.txt
        New-Item -Path $workingdir\correlationids\http.csv -Force 
        $res="device,correlationid"
        $res |  Out-File "$workingdir\correlationids\http.csv" -Append -Force -Encoding UTF8
        foreach($h in $httpsamples)
        {
            Write-Host "$h"
            $http_sub = submit-malwareapi -captureid $h -product $productcategory
            $cor_id = $($http_sub.correlationId)
            $res="$global:device,$cor_id"
            $res |  Out-File "$workingdir\correlationids\http.csv" -Append -Force -Encoding UTF8
        }
    }elseif ($samples -eq 'fp') {
        If(Test-Path $workingdir\correlationids\falsepositive.csv)
        {
            Rename-Item -Path $workingdir\correlationids\falsepositive.csv -NewName "falsepositive_$dte.csv" -Confirm:$false -Force
        }
        $fpsamples=Get-Content $workingdir\samples\falsepositive.txt
        New-Item -Path $workingdir\correlationids\falsepositive.csv -Force 
        $res="device,correlationid"
        $res |  Out-File "$workingdir\correlationids\falsepositive.csv" -Append -Force -Encoding UTF8
        foreach($f in $fpsamples)
        {
            Write-Host "$f"
            $fp_sub = submit-malwareapi -captureid $f -product $productcategory
            $cor_id = $($fp_sub.correlationId)
            $res="$global:device,$cor_id"
            $res |  Out-File "$workingdir\correlationids\falsepositive.csv" -Append -Force -Encoding UTF8
        }
    }elseif ($samples -eq 'exploit') {
        If(Test-Path $workingdir\correlationids\exploit.csv)
        {
            Rename-Item -Path $workingdir\correlationids\exploit.csv -NewName "exploit_$dte.csv" -Confirm:$false -Force
        }
        $exploitsamples=Get-Content $workingdir\samples\exploit.txt
        New-Item -Path $workingdir\correlationids\exploit.csv -Force 
        $res="device,correlationid"
        $res |  Out-File "$workingdir\correlationids\exploit.csv" -Append -Force -Encoding UTF8
        foreach($ex in $exploitsamples)
        {
            Write-Host "$ex"
            $ex_sub = submit-exploitapi -captureid $ex -product $productcategory
            $cor_id = $($ex_sub.correlationId)
            $res="$global:device,$cor_id"
            $res |  Out-File "$workingdir\correlationids\exploit.csv" -Append -Force -Encoding UTF8
        }
    }else {
        Write-Host "No such $sample list available."
    }


    
    New-Item -Path $workingdir\correlationids\email.csv -Force 
    $res="device,correlationid"
    $res |  Out-File "$workingdir\correlationids\email.csv" -Append -Force -Encoding UTF8
    foreach($e in $emailsamples)
    {
        Write-Host "$e"
        $email_sub = submit-api -captureid $e -product $product -filename "submit-malwarerequest"
        $cor_id = $($email_sub.correlationId)
        $res="$product,$cor_id"
        $res |  Out-File "$workingdir\correlationids\email.csv" -Append -Force -Encoding UTF8
    }

    $device = $global:device
    foreach($content in $contents)
    {
        Write-Host "Submitting API Request for $content : $device"
        submit-api -product $device -captureid $content
    }



}
