﻿#First, create secure string of your password pwd.txt file using below code
#"Password" | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString | Out-File "C:\SendEmailPS\pwd.txt"

# Provide Variables
[string]$MailReportEmailFromAddress = "Recipents@labs.com"
[string]$MailReportEmailToAddress = "abc@labs.com"
[string]$MailReportEmailSubject = "Testing Only "
[string]$MailReportSMTPServer = "11.111.111.11"
[int32]$MailReportSMTPPort = "25"
[boolean]$MailReportSMTPServerEnableSSL = $False

#Email the Report

function MailReport {
    param (
        [ValidateSet("TXT","HTML")] 
        [String] $MessageContentType = "HTML"
    )
    $message = New-Object System.Net.Mail.MailMessage
    $mailer = New-Object System.Net.Mail.SmtpClient ($MailReportSMTPServer, $MailReportSMTPPort)
    $mailer.EnableSSL = $MailReportSMTPServerEnableSSL
    if ($MailReportSMTPServerUsername -ne "") {
        $File = "C:\SendEmailPS\pwd.txt"
        $cred=New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User, (Get-Content $File | ConvertTo-SecureString)
        $mailer.Credentials = New-Object System.Net.NetworkCredential($cred.UserName, $cred.Password)
    }
    $message.From = $MailReportEmailFromAddress
    $message.To.Add($MailReportEmailToAddress)
    $message.Subject = $MailReportEmailSubject
    $message.IsBodyHtml = $True
    $body="<body><p><span style='font-size: 20pt;color:blue'>Test Email</span></p><br><p><span style='font-family: courier;font-size: 12pt;color:red'>Good Day :)</span></p></body>"
    $message.Body = $body
    $attachment = "C:\SendEmailPS\test_doc.txt"
    $message.Attachments.Add($attachment)
    $mailer.send(($message))
}

MailReport






