function update-exploitdevices{
    param($device, $enable)
    $dbun = $global:settings.oltpdb_un
    $dbpw = $global:settings.oltpdb_pw
    $serverinstance = $global:settings.serverinstance
    $dbreplay=$global:settings.schema_replay_exp
    $dbvictim=$global:settings.schema_victim_exp
    $db=$global:settings.db_exp

    Write-Host "Updating the Devices in the Database : $vector" -ForegroundColor Yellow
    if($enable -eq 'yes')
    {
        $query= "Update $dbvictim  set Enabled=1  Where Device in ('$device') and ((VictimName not like '%-B39') and (VictimName not like '%-B40'))" 
    }else {
        $query= "Update $dbvictim  set Enabled=0  Where Device in ('$device')" 
    }
    Invoke-SQLcmd -ServerInstance $serverinstance -query $query -Username $dbun -Password $dbpw -Database $db
    Start-Sleep -Seconds 5

    $query_count = "Select ISNULL(Count(*),0) As Count from $dbvictim Where Device in ('$device') and Enabled=1"
    $count = $(Invoke-SQLcmd -ServerInstance $serverinstance -query $query_count -Username $dbun -Password $dbpw -Database $db).Count

    if($enable -eq "yes")
    {
        if($count -ge 1)
        {
            Write-Host "$device successfully updated on $dbvictim " -ForegroundColor Blue
        }
        else 
        {
            Write-Host "$device update failed on $dbvictim " -ForegroundColor Red
        }
    }
    elseif ($enable -eq "no") 
    {
        if($count -ge 1)
        {
            Write-Host "$device update failed on $dbvictim " -ForegroundColor Red
        }
        else 
        {
            Write-Host "$device successfully updated on $dbvictim " -ForegroundColor Blue
        }
    }

    $enable=$null
    $query=$null
}

Export-ModuleMember -Function update-exploitdevices