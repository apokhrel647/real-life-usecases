$wsusScript = @"
hostname
`$wsusRegistryKeys = @(
    @("HKLM:SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU","NoAutoUpdate","DWORD","1","Disable AutoUpdate"),
    @("HKLM:Software\Policies\Microsoft\Windows\WindowsUpdate","DisableWindowsUpdateAccess","DWORD","1","Disable Windows Update Access"),
    @("HKLM:Software\Policies\Microsoft\Windows\WindowsUpdate","WUServer","STRING","http://wsus-01.colo1.labs.com:8530","WSUS Server URL"),
    @("HKLM:Software\Policies\Microsoft\Windows\WindowsUpdate","WUStatusServer","STRING","http://wsus-01.colo1.labs.com:8530","WSUS Status Server URL"),
    @("HKCU:Software\Microsoft\Windows\CurrentVersion\Policies\Explorer","NoWindowsUpdate","DWORD","1","Disable Windows Update from IE"),
    @("HKLM:SYSTEM\Internet Communication Management\Internet Communication","DisableWindowsUpdateAccess","DWORD","1","Disable Windows Update features"),
    @("HKCU:Software\Microsoft\Windows\CurrentVersion\Policies\WindowsUpdate","DisableWindowsUpdateAccess","DWORD","1","Disable Windows Update features-2"),
    @("HKLM:Software\Policies\Microsoft\Windows\WindowsUpdate\AU","AUOptions","DWORD","1","Windows update notify before download"),
    @("HKLM:Software\Policies\Microsoft\Windows\WindowsUpdate\AU","UseWUServer","DWORD","1","Enable WSUS")
)

function set-RegistryKey {
    param (
        [Parameter (mandatory=`$true)][AllowEmptyString()][string[]] `$regKey 
    )
    if (-not (test-path -path `$regKey[0])) {
        `$keyPath = (`$regKey[0].Split(':'))[0]
        `$keyPath += ":"
        `$keys = (`$regKey[0].Split(':'))[1].Split('\')
        foreach (`$key in `$keys) { 
            `$keyPath += "`$key"
            if(-not (test-path -path `$keyPath)) {
                #Write-Host "****`$keyPath****"
                New-Item -Path `$keyPath | Out-NUll 
            }
            `$keyPath += "\"
        }

    }
    `$realKeyPath = `$regKey[0]
    `$keyProperties = Get-ItemProperty `$realKeyPath
    if (`$null -ne `$keyProperties -and (Get-Member -InputObject `$keyProperties -Name `$regKey[1])) {
        Set-ItemProperty -Path `$realKeyPath -Name `$regKey[1] -Value `$regkey[3] | Out-Null
    } else {
        New-ItemProperty -Path `$realKeyPath -Name `$regKey[1] -PropertyType `$regKey[2] -Value `$regkey[3] | Out-Null
    }
}

foreach (`$key in `$wsusRegistryKeys) {
    set-RegistryKey -regKey `$key
}
"@