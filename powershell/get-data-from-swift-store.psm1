function get-swiftdata{
   param($filename)
   function get-swiftApiKey {
       #First make the connection to swift
       [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
       $apiUser = $global:apiUser
       $apiPasswd = $global:apiPasswd
       #$authURI = 'https://onrswift-api.colo1.tech.com/auth/v1.0'
       $authURI = 'https://swiftstack.tech.com/auth/v1.0'
       $authHeaders = @{'X-Auth-User'=$apiUser;'X-Auth-Key'=$apiPasswd} 
       $authResponse = Invoke-WebRequest -Uri $authURI -Headers $authHeaders -UseBasicParsing
       if ($authResponse.headers['X-Auth-Token']) {
           return $authResponse.headers['X-Auth-Token']
       } else {
           throw "Unable to log into swift"
       }
   }
   
   $swift="" | Select-Object -Property pcap, logging
   $res=$null
   $limit = 3
   $container = $global:swiftcontainer
   #$queryURI = "https://onrswift-api.colo1.tech.com/v1/AUTH_pep_swift/$($container)?format=json&limit=$limit&prefix=$filename"
   #$queryURI = "https://swiftstack.tech.com/v1/AUTH_pep_swift/$($container)?format=json&limit=$limit&prefix=$filename"
   $queryURI = "https://swiftstack.tech.com/v1/AUTH_pout_swift/$($container)?format=json&limit=$limit&prefix=$filename"
   $apiKey = get-swiftApiKey
   $queryHeaders = @{'X-Auth-Token' = $apiKey}
   $swiftObjects = Invoke-RestMethod -Uri $queryURI -Method Get -Headers $queryHeaders -ErrorAction SilentlyContinue -UseBasicParsing
   $res = $($swiftObjects) 
   #Write-Host $res
   #Write-Host $filename
   if($res -like "*name=$filename/PCAPTraffic.zip*" )
   {
       $swift.pcap=1
   }else 
   {
       $swift.pcap=0
   }
   if($res -like "*$filename/DeviceLogging.zip*" )
   {
       $swift.logging=1
   }else 
   {
       $swift.logging=0
   }
   return $swift
   
}

Export-ModuleMember -Function get-swiftdata

# $sessiontoken="1f46dd0baa9044eb9fb6977a"
# get-swiftdata -filename $sessiontoken