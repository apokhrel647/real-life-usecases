[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$records = Get-VM "VM*"
#$vms= Get-VM -Location "Win10Patched" | sort

# Graceful shutdown
get-vm $($records.VMNAME) | Where {$_.PowerState -ne "PoweredOff"} | Shutdown-VMGuest -Confirm:$false

# Hard shutdown
get-vm $($records.VMNAME) | Where {$_.PowerState -ne "PoweredOff"} | Stop-VM -Confirm:$false

# Power on
Start-VM $($records.VMNAME)

# list PoweredOn vms
get-vm $($records.VMNAME) | Where {$_.PowerState -ne "PoweredOff"}

# Change VMs to Independent Persistent mode
Get-HardDisk -VM $($records.VMNAME) | Set-HardDisk -Persistence 'IndependentPersistent' -Confirm:$false

# Change VMs to Independent Non Persistent mode
Get-HardDisk -VM $($records.VMNAME) | Set-HardDisk -Persistence 'IndependentNonPersistent' -Confirm:$false

# List VMs that are in Independent Non Persistent mode
get-vm $($records.VMNAME) | Get-HardDisk | where {$_.Persistence -ne "IndependentNonPersistent"}

# List VMs that are in Independent Persistent mode
get-vm $($records.VMNAME) | Get-HardDisk | where {$_.Persistence -ne "IndependentPersistent"}

### DELETE LANES

function delete_lane{
    param($deletinglane)
    $totalcnt=0
    $vms=$(Get-VM "$deletinglane-*").Name | sort
    get-vm $vms | Where {$_.PowerState -ne "PoweredOff"} | Stop-VM -Confirm:$false
    $totalcnt=$($vms.count)
    ##Delete Victims
    $sn=0
    foreach($v in $vms)
    {   $sn=$sn+1
        Remove-VM -VM $v -DeletePermanently -RunAsync -Confirm:$false
        Write-Host "$sn / $totalcnt : $v Deleted" -ForegroundColor DarkGreen
    }
}

$ged=Get-Credential
Connect-VIServer onrvcsa-pep.mgt.colo1.labs.com -Protocol https -Credential $ged
$deletinglanes= "Lane0"
$deletinglane=$null
foreach($deletinglane in $deletinglanes){
    delete_lane -getcredential $ged -deletinglane $deletinglane
}


##Clean Scheduled Tasks of Remote VMS
$script=@"
`$TS=New-Object -ComObject Schedule.Service
`$TS.Connect(`$env.COMPUTERNAME)
`$TaskFolder=`$TS.GetFolder("\")
`$Tasks=`$TaskFolder.GetTasks(1)
foreach(`$Task in `$Tasks)
{
    `$TaskFolder.DeleteTask(`$Task.Name,0)
}
"@
Invoke-VMScript -VM $($records.VMNAME) -ScriptText $script -GuestUser FakeUserName-GuestPassword FakePassword -ScriptType Powershell


Copy-VMGuestFile -Source "C:\Users\user\Desktop\aTf57Qj8aY8WLBAG.txt" -Destination "C:\Users\FakeUsername\Desktop\" -Force -VM $vms -LocalToGuest -GuestUser $remoteuname -GuestPassword $remotepwd -Confirm:$false

## Delete remote files
$script='Remove-Item C:\Users\FakeUsername\Desktop\Protect_x64.msi -force -Confirm:$false'
Invoke-VMScript -VM $vms -ScriptText $script -GuestUser FakeUserName -GuestPassword FakePassword -ScriptType Powershell -RunAsync


#Run PS Script on remote vm
$theScript = "powershell.exe -executionpolicy bypass -file 'C:\Program Files\WinLogBeat\install-winlogbeat.ps1 ' "
Invoke-VMScript -VM $vms.Name -ScriptType Powershell -ScriptText $theScript -GuestUser FakeUserName -GuestPassword FakePassword

#Delete VMS permanently
get-VM "A04-B01" | Remove-VM -Confirm:$false -DeletePermanently










