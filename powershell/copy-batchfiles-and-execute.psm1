function upload-chocofiles
{
    param($device,$poweronrequired)

    if($($DefaultVIServer.Name) -ne "onrvcsa-pep01.mgt.colo1.labs.com")
    {
        Connect-VIServer onrvcsa-pep01.mgt.colo1.labs.com -Protocol https -Credential $global:cred
    }
    $remoteuname="FakeUsername"
    $remotepwd="FakePassword"
    
    $victims = $($global:victims_exploit | Where-Object {$_.Product -eq $device}).VMName
    if($poweronrequired -eq "yes")
    {
        foreach($v in $victims)
        {
            Start-VM $v -RunAsync -Confirm:$false | Out-Null
        }
        Write-Host "Sleeping for 2 Minutes"
        Start-Sleep -Seconds 120
    }
    foreach($victim in $victims)
    {
	#Copying the files
        Copy-VMGuestFile -Source "$global:workingdir\chocoFiles\*.bat" -Destination "C:\CN_CORE\choco\" -VM "$victim" -LocalToGuest -GuestUser $remoteuname -GuestPassword $remotepwd -Force
	
	#Executing Batch Files
        $res_afp =Invoke-VMScript -ScriptText "Test-Path -Path C:\CN_CORE\choco\choco-afp.bat" -ScriptType Powershell -VM "$victim" -GuestUser $remoteuname -GuestPassword $remotepwd -Confirm:$false
        $res_ard =Invoke-VMScript -ScriptText "Test-Path -Path C:\CN_CORE\choco\choco-ard.bat" -ScriptType Powershell -VM "$victim" -GuestUser $remoteuname -GuestPassword $remotepwd -Confirm:$false
        $res_java =Invoke-VMScript -ScriptText "Test-Path -Path C:\CN_CORE\choco\choco-java.bat" -ScriptType Powershell -VM "$victim" -GuestUser $remoteuname -GuestPassword $remotepwd -Confirm:$false
        $res_silverlight =Invoke-VMScript -ScriptText "Test-Path -Path C:\CN_CORE\choco\choco-silverlight.bat" -ScriptType Powershell -VM "$victim" -GuestUser $remoteuname -GuestPassword $remotepwd -Confirm:$false
        if(($res_afp -match "True") -or ($res_ard -match "True") -or ($res_java -match "True") -or ($res_silverlight -match "True"))
        {
            Write-Host "Upload of Chocolatey Batch Files to $victim C:\CN_CORE\choco\ is successful." -ForegroundColor Blue   
        }
        else {
            Write-Host "Upload of Chocolatey Batch Files to $victim C:\CN_CORE\choco\ is not successful." -ForegroundColor Red
        }

    }
    Write-Host "Upload Complete" -ForegroundColor Yellow

}


Export-ModuleMember -Function upload-chocofiles