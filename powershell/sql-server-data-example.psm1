function get-chococheck
{
    $dbun = $global:settings.oltpdb_un
    $dbpw = $global:settings.oltpdb_pw
    $serverinstance = $global:settings.serverinstance
    $dbreplay=$global:settings.schema_replay_exp
    $dbvictim=$global:settings.schema_victim_exp
    $db=$global:settings.db_exp

    $active_devices = $global:distinctdevices_exploit.Product
    $active_victims = $global:victims_exploit.VMName

    Write-Host "`nChocolatey Based Applications Check (Only from the Data Perspective)" -ForegroundColor Yellow
    if($($active_devices.Count) -lt 1)
    {
        Write-Host "$dbvictim : None of the devices are enabled." -ForegroundColor Blue     
    }
    else 
    {
        foreach($victim in $active_victims)
        {
            $query="SELECT VictimDevice,VictimName, COUNT(CASE WHEN (Blocked IS NOT NULL and PcapTrafficFileIdentifier IS NOT NULL and UniversalIntroductionDate IS NOT NULL and ErrorMessage IS NULL) THEN 1 ELSE NULL END) As Count_GoodRecords
            ,COUNT(CASE WHEN ((Blocked IS NULL or PcapTrafficFileIdentifier IS NULL or UniversalIntroductionDate IS NULL) and ErrorMessage IS NOT NULL) THEN 1 ELSE NULL END) As Count_BadRecords
            FROM [Replay_Smoke].[dbo].[Replay] With(NoLock)
            Where VictimName='$($victim)' and Id>$global:maxid_exp
            and NSSId IN (Select Distinct NSSId FROM [Replay_Smoke].[dbo].[Capture] With(NoLock)
            Where TargetedApplication NOT IN ('Internet Explorer 9','Internet Explorer 10','Internet Explorer 11'))
            Group BY VictimDevice,VictimName
            Order By VictimName"
            $records = $(Invoke-SQLcmd -ServerInstance $serverinstance -query $query -Username $dbun -Password $dbpw -Database $db)
            $rec = $global:victims_exploit | Where-Object {$_.VMName -eq $($victim)}
            if(!$records)
            {
                Write-Host "$($rec.Product),$($rec.VMName),$($rec.OS),$($rec.Browser),CountGoodRecords:NA,CountBadRecords:NA" -ForegroundColor Green
            }
            else 
            {
                if($($records.Count_BadRecords) -lt 1)
                {
                    Write-Host "$($rec.Product),$($rec.VMName),$($rec.OS),$($rec.Browser),CountGoodRecords:$($records.Count_GoodRecords),CountBadRecords:$($records.Count_BadRecords)" -ForegroundColor Blue
                }
                else 
                {
                    Write-Host "$($rec.Product),$($rec.VMName),$($rec.OS),$($rec.Browser),CountGoodRecords:$($records.Count_GoodRecords),CountBadRecords:$($records.Count_BadRecords)" -ForegroundColor Red
                }
                
            }

        }

    }
}

Export-ModuleMember -Function get-chococheck