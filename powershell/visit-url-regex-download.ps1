[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

## This is just a sample to show how you can visit the URLs, match the Regex and download the files, update to MS SQL Server DB

$family='Chrome'
#Query that pulls the last updated date  ## and id=1538
$last_query="
SELECT TOP 1 [_LoadDate] FROM [DB].[dbo].[Application] Where Family='Chrome' and id=1538 Order By Id DESC
GO
"
#URL from RSS Feed to get the latest Google Chrome update published date & Last Updated Date
$url_rss_chrome="https://chromereleases.googleblog.com/2019/02/stable-channel-update-for-desktop_21.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+GoogleChromeReleases+%28Google+Chrome+Releases%29&utm_content=FeedBurner"
$regex_chrome='The stable channel has been updated to ([0-9]+[.0-9]+) for Windows, Mac, and Linux'
$update_status=get_update_status -last_query $last_query -url_rss $url_rss_chrome

$url32dl_chrome="https://cloud.google.com/chrome-enterprise/browser/download/?utm_source=Chrome_Consumer_Site&utm_medium=website&utm_campaign=2017-17q4-gc-devices-chromeaware-chrome-direct-gdn-leadgen-chromeenterprise&utm_content=ChromeEnterprise&utm_term=EnterpriseHeader#/googlechromestandaloneenterprise.msi"
$url64dl_chrome="https://cloud.google.com/chrome-enterprise/browser/download/?utm_source=Chrome_Consumer_Site&utm_medium=website&utm_campaign=2017-17q4-gc-devices-chromeaware-chrome-direct-gdn-leadgen-chromeenterprise&utm_content=ChromeEnterprise&utm_term=EnterpriseHeader#/googlechromestandaloneenterprise.msi"
$path="C:\UsefulPowershellScripts\ChocolateyPackages\Installers"
$installer32_chrome="chrome32bits_$version.msi"
$installer64_chrome="chrome64bits_$version.msi"


## Returns either 0 or 1
function get_update_status
{
    #cat function: "Get Published & Last Updated Dates"
    param($url_rss, $last_query)

    $html=invoke-webrequest -uri $url
    $pub_date = Get-Date($($html.parsedhtml.getelementsbyclassname("publishdate")).textContent) -UFormat "%Y%m%d"
    [int]$int_pub = [convert]::ToInt32($pub_date,10)
    #Write-Host "Published date " $int_pub

    $last_updated_date = Invoke-SQLcmd -ServerInstance 'HostName\MSSQLSERVER,1433' -query $last_query -Username FakeUserName -Password FakePassword -Database DB 
    $lup=Get-Date $($last_updated_date[0]) -UFormat "%Y%m%d"
    [int]$int_last_up_date = [convert]::ToInt32($lup,10)
    
    if($int_pub -gt $int_last_up_date)
    {
        #Write-Host "New Update available" -ForegroundColor Blue -BackgroundColor White
        return $up_status=1
    }
    else
    {
        #Write-Host "No new Update available" -ForegroundColor Red -BackgroundColor White
        return $up_status=0
    }


}

## Visit the URK and match the Regex
function get_version_info
{
    param($url_rss, $regex)

    #URL from RSS Feed to get the latest Google Chrome update published date 
    $html=invoke-webrequest -uri $url_rss
    $string = $($html.parsedhtml.getelementsbyclassname("post-content")).InnerHTML
    $matches=$($string | Select-String -Pattern $regex).Matches.Groups[1]
    return $matches.Value
}

# Download the Binaries
function get_binaries_downloaded
{
    param($url32dl,$url64dl,$path,$installer32,$installer64)

    #Direct URLs from Google website to download latest releases into $Path folder
    #Return 0 & 2 for Failure & 1 & 3 for Success
    Invoke-WebRequest $url32dl -OutFile $path\$installer32
    Invoke-WebRequest $url64dl -OutFile $path\$installer64
    If(!(Test-Path "$path\$installer32") -or !(Test-Path "$path\$installer64")) 
        {return "False"} 
    else 
        {return "True"}
}

