function set-VMTag {
param (
    [string[]]$vmName,
    [string]$tagName,
    [string]$tagValue
)

#Verify that the tage exists
$tag = Get-CustomAttribute -Name $tagName -ErrorAction SilentlyContinue

if ($null -eq $tag) {
    $tag= New-CustomAttribute -Name $tagName -TargetType 'VirtualMachine'
}

$vms = Get-VM -Name $vmName
Set-Annotation -Entity $vms -CustomAttribute $tag -Value $tagValue | Out-Null
    
}