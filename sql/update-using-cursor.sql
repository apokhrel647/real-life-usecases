Declare c cursor for SELECT [Id],IntroDate
FROM [DB1].[dbo].[Table1]
Where Id IN (2027828,2027814);
Declare @id INT;
Declare @date datetime;
open c;

fetch next from c into @id, @date;
While @@FETCH_STATUS=0
Begin
	Update [11.111.111.111].[DB1].[dbo].[Table1] Set IntroDate = @date Where VectorType='Malware' and BId=@id ;
	Update [11.111.111.111].[DB1].[dbo].[Table1] Set ExecDate = @date Where VectorType='Malware' and BId=@id ;
	Update [11.111.111.111].[DB1].[dbo].[Table1] Set AcqDate = @date Where VectorType='Malware' and BId=@id ;
	Update [11.111.111.111].[DB1].[dbo].[Table1] Set BotDate = @date Where VectorType='Malware' and BId=@id ;
	print @id 
	fetch next from c into @id, @date;
end;
close c;
deallocate c;