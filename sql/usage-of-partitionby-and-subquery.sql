Select Count(Distinct Id) As "No of Id"
,COUNT(CASE WHEN ([Dispersion(Minutes)]<=2 and NoOfDevices=22) THEN 1 ELSE NULL END) AS "No of Intersected Id With Dispersion<=2 Minutes"
,COUNT(CASE WHEN ([Dispersion(Minutes)]>2 and NoOfDevices=22) THEN 1 ELSE NULL END) AS "No of Intersected Id With Dispersion>2 Minutes"
,COUNT(CASE WHEN (NoOfDevices<22) THEN 1 ELSE NULL END) AS "No of UnIntersected Id"
From
( 
	SELECT [Id],Count(Distinct Device) as "NoOfDevices",DATEDIFF(minute,Min(UniversalIntroductionDate),Max(UniversalIntroductionDate)) As "Dispersion(Minutes)"
	FROM [DB1].[dbo].[Table1] With(NoLock) 
	Where ControllerGroup='Exploit'
	and (PcapTrafficFileIdentifier is NOT NULL and LocalIntroductionDate is NOT NULL and Blocked is NOT NULL)
	and Id IN
		(Select Distinct Id
		From
		(
		select r.Id, r.uri, r.Platform,r.Browser,r.Blocked, ROW_NUMBER() OVER (PARTITION BY r.uri, r.Platform,r.Browser ORDER BY r.localintroductiondate) rank
		from [DB1].[dbo].[Table1] r With(NoLock) 
		where (r.ControllerGroup like 'Exploit' and r.Device='BaseLine' and r.uri Not like ('%wicar.org%'))
		and r.Blocked=0
		) R1 
		Where R1.rank=1)
Group By Id)R1