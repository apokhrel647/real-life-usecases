SELECT [Id] ,d.Name As "Device",(CASE WHEN Blocked=0 THEN 1 ELSE 0 END) As "Miss"
FROM [DB1].[dbo].[Table1] r inner join [DB1].[dbo].[Device] d on r.DeviceId=d.Id
Where dateid BETWEEN 20171201 AND 20180903
and d.Name IN ('IBM','Juniper')
and Id IN
(
'2017-4CKVQ6','2017-4CKVQ8',
)
Group By Id, d.Name, Blocked
Order By d.Name, Id, Blocked